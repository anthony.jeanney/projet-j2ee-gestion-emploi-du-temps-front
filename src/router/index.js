import { createRouter, createWebHistory } from 'vue-router'
import TimetableView from '../views/TimetableView.vue'
import ManageView from '../views/ManageView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'Emploi du temps',
      component: TimetableView
    },
    {
      path: '/gestion',
      name: 'Gestion',
      component: ManageView
    }
  ]
})

export default router
