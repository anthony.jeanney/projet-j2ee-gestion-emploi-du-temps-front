export function parseDate(date) {
    const timezoneOffset = date.getTimezoneOffset() * 60000
    return new Date(date - timezoneOffset).toISOString().slice(0, 16)
}