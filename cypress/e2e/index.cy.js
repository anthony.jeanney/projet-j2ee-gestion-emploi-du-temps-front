describe('template spec', () => {
  beforeEach('When i go to the index page', () => {
    cy.intercept('GET', '**/lieux/', { fixture: 'locations.json' })  // replace api response with fixture data
    cy.intercept('GET', '**/promotion', { fixture: 'promotions.json' })
    cy.intercept('GET', '**/creneaux/promotion/**', { fixture: 'timeslots.json' })
    cy.intercept('GET', '**/matiere', { fixture: 'matieres.json' })
    cy.clock(Date.UTC(2024, 2, 6, 0, 0, 0), ['Date'])
    cy.visit('http://localhost:5173')
  })

  it('Timetable should be empty', () => {
    cy.get('.toastui-calendar-event-time').should('not.exist')
  })

  it('There should be some promotions', () => {
    cy.get('.navigation')
        .find('select')
        .children()
        .should('have.length.greaterThan', 1);
  })

  it('Timetable should have some events', () => {
    cy.get('.form-select').select('1')
    cy.get('.toastui-calendar-event-time').should('exist').and('be.visible')
  })
})